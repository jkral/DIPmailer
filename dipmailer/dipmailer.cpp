
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include "Dip.h"
#include "DipSubscription.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <ctime>
#include <termios.h>
#include <unistd.h>
#include "CSmtp.h"


using namespace std;


// trim from start
static inline string &ltrim(string &s) {
	s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
	return s;
}

// trim from end
static inline string &rtrim(string &s) {
	s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
	return s;
}

// trim from both ends
static inline string &trim(string &s) {
	return ltrim(rtrim(s));
}

void SetStdinEcho(bool enable = true)
{
#ifdef WIN32
    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE); 
    DWORD mode;
    GetConsoleMode(hStdin, &mode);

    if( !enable )
        mode &= ~ENABLE_ECHO_INPUT;
    else
        mode |= ENABLE_ECHO_INPUT;

    SetConsoleMode(hStdin, mode );

#else
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    if( !enable )
        tty.c_lflag &= ~ECHO;
    else
        tty.c_lflag |= ECHO;

    (void) tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}


/**
* Simple demo client that subscribes to data coming from servers.
* Subscribe to "dip/test/api/dip-server_<from 0 to noPubs>"
*/

class Client {

private:
	// hold reference to subscription objects
	DipSubscription *sub;

	// DIP object
	DipFactory *dip;

	/**
	* handler for connect/disconnect/data reception events
	* Nested class
	* */
	class GeneralDataListener:public DipSubscriptionListener{
	private:

		// allow us to access subscription objects
		Client * client;

	//	bool flip;

	public:
		GeneralDataListener(Client *c):client(c){};

		/**
		* handle changes to subscribed to publications
		* Simply prints the contents of the received data.
		* @param subscription - the subsciption to the publications thats changed.
		* @param message - object containing publication data
		* */
		void handleMessage(DipSubscription *subscription, DipData &message){

			bool bError = false;
			bool mailfrom = false;
			bool mailto = false;
			string state;
			string msgstr;
			CSmtp *mail;
		
			mail = 0;
			
			string newstate;
			
			newstate = message.extractString("value");
			
			time_t now = time(0);
			char* dt = ctime(&now);
		
			cout << endl;
			cout << dt;
			cout<<"Received data from: " << subscription->getTopicName() << "  value: " << newstate << endl;
			
			// check the state list if to mail
			istringstream is_to( client->transitto );
			
			while( getline( is_to, state, ',' )){
				trim( state );
				if( newstate == state )
					mailto = true;
			}
			
			istringstream is_from( client->transitfrom );
			
			while( getline( is_from, state, ',' )){
				trim( state );
				if( client->laststate == state )
					mailfrom = true;
			}

			// mail
			if(( mailfrom == true || mailto == true ) && client->firstPass == false ){
				// mail the state enter/exit message
				try{
					mail = new CSmtp();
					
					mail->SetSMTPServer( client->smtpserver.c_str(), client->smtpport );
					mail->SetSecurityType( USE_TLS );

					mail->SetLogin( client->smtplogin.c_str() );
					mail->SetPassword( client->smtppasswd.c_str() );
					mail->SetSenderName( "DIP mailer" );
					mail->SetSenderMail( client->sender.c_str() );
					mail->SetReplyTo( client->sender.c_str() );
					mail->SetSubject( "Beam transit" );
					mail->AddRecipient( client->recipient.c_str() );
					mail->SetXPriority( XPRIORITY_NORMAL );
					mail->SetXMailer( "The Bat! (v3.02) Professional" );
					if( mailfrom ){
						msgstr = "Beam exited " + client->laststate + ".";
						mail->AddMsgLine( msgstr.c_str() );
					}
					if( mailto ){
						msgstr = "Beam entered " + newstate + ".";
						mail->AddMsgLine( msgstr.c_str() );
					}
					// time info
					mail->AddMsgLine( dt );
					
					mail->Send();
					
				}
				catch(ECSmtp e)
				{
					cout << "Error: " << e.GetErrorText().c_str() << ".\n";
					bError = true;
				}
				if( !bError )
					cout << "Mail was send successfully.\n";
				
				if( mail != 0 )
					delete mail;
			}

			// save state
			client->laststate = newstate;
			
			// first pass frotection
			client->firstPass = false;
			
		}


		/**
		* called when a publication subscribed to is available.
		* @param arg0 - the subsctiption who's publication is available.
		* */
		void connected(DipSubscription *arg0) {
			cout << "\nPublication source  " << arg0->getTopicName()<< " available\n";
		}


		/**
		* called when a publication subscribed to is unavailable.
		* @param arg0 - the subsctiption who's publication is unavailable.
		* @param arg1 - string providing more information about why the publication is unavailable.
		* */
		void disconnected(DipSubscription *arg0, char *arg1) {
			//printf("\nPublication source %s unavailable\n", arg0->getTopicName());
		}

		void handleException(DipSubscription* subscription, DipException& ex){
			//printf("Subs %s has error %s\n", subscription->getTopicName(), ex.what());
		}

	};

	//A handle on the DIP Data recipient.
	GeneralDataListener *handler;

public:

	/**
	* set up the subscriptions to dip/test/pub1 dip/test/pub2
	* */
	Client(const int argc, const char ** argv)
	{
		dip = Dip::create( argv[1] );
		handler = new GeneralDataListener(this);
		//Creating an array of DipSubscriptions.
		dip->setDNSNode("dipnsgpn1");
				
			
		ostringstream oss;
		oss << "dip/acc/LHC/RunControl/BeamMode";
		sub = dip->createDipSubscription(oss.str().c_str(), handler);
		
		laststate = "x";
		firstPass = true;
	}

	~Client(){
		dip->destroyDipSubscription(sub);
		delete handler;
		delete dip;
		cout<<"Client's done."<<endl;
	}
	
	string sender;
	string recipient;
	string smtpserver;
	int smtpport;
	string smtplogin;
	string smtppasswd;
	string transitto;
	string transitfrom;
	
	string laststate;
	
	bool firstPass;
};


/**
* Start the client
* */
int main(const int argc, const char ** argv){

	
	if( argc == 1 ){
		cout << "usage: " << argv[0] << " <client name>" << endl;
		cout << "  where <client name> is your custom and unique ID string" << endl;
		cout << "  where [config file] path to config file (default: ./dipmailer.conf)" << endl;
		return 0;
	}
	
	Client * theClient = new Client(argc,argv);

	sleep( 3 );
	
	ifstream is_file;
	string line;
	bool doexit;
	char keystroke;
	string cfile;

	// read configuration
	if( argc > 2 )
		cfile = argv[2];
	else
		cfile = "dipmailer.conf";

	is_file.open( cfile.c_str(), ifstream::in );

	if( ! is_file.is_open() ){
		cout << "Error opening config file " << cfile << endl;
		return 0;
	}
	
	
	while( getline( is_file, line )){
		string key;
		istringstream is_line( line );
		if( getline( is_line, key, '=' ))
		{
			key.erase( std::remove_if( key.begin(), key.end(), ::isspace ), key.end() );
			if( key[0] == '#' )
				continue;
			
			string value;
			if( getline(is_line, value) ){
				
				trim( key );
				//value.erase( std::remove_if( value.begin(), value.end(), ::isspace ), value.end() );
				
				// decode config
				if( key == "sender" )
					theClient->sender = value;
				if( key == "recipient" )
					theClient->recipient = value;
				if( key == "smtpserver" )
					theClient->smtpserver = value;
				if( key == "smtpport" )
					theClient->smtpport = atoi( value.c_str() );
				if( key == "smtplogin" )
					theClient->smtplogin = value;
				if( key == "smtppasswd" )
					theClient->smtppasswd = value;
				if( key == "transitto" )
					theClient->transitto = value;
				if( key == "transitfrom" )
					theClient->transitfrom = value;

			}

			if( key == "smtppasswd" ){

				if( value == "" ){
					cout << "smtp passwd: ";
					SetStdinEcho( false );
					cin >> value;
					cin.get();
					SetStdinEcho( true );
					theClient->smtppasswd = value;
				}
				
			}
		}
	}
	
	is_file.close();
	
	// config printout
	cout << endl;
	cout << "Config ===========================================" << endl;
	cout << "sender = " << theClient->sender << endl;
	cout << "recipient = " << theClient->recipient << endl;
	cout << "smtp server = " << theClient->smtpserver << endl;
	cout << "smtp port = " << theClient->smtpport << endl;
	cout << "smtp login = " << theClient->smtplogin << endl;
	cout << "transit to = " << theClient->transitto << endl;
	cout << "transit from = " << theClient->transitfrom << endl;
	cout << "==================================================" << endl;
	cout << endl;
	
	cout << "type x <return> to exit" << endl;
	doexit = false;
	
	while( doexit == false ){
		sleep( 1 );
		cin >> keystroke;
		if( keystroke == 'x' )
			doexit = true;
	}

	delete theClient;
	return(0);
}

