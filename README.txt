# DIP client with SSL/TLS e-mail option
# define BeamMode(s) and recieve e-mail on transition from/to the mode(s)
# Jiri Kral jiri.kral@cern.ch 2016


1) source ./setenv.sh

2) cd dipmailer

3) make clean; make

4) edit dipmailer.conf

5) ./dipmailer <client name> [config file]
  where <client name> is unique custom ID string
  where [config file] path to config file (default: ./dipmailer.conf)

6) type x<return> to exit the dipmailer

